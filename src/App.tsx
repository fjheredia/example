import React, { useState } from 'react';
import './App.css';

interface Car {
  id: number;
  brand: string;
  model: string;
  year: number;
  image: string;
}

const carData: Car[] = [
  { id: 1, brand: 'Toyota', model: 'Corolla', year: 2020, image: './image/corolla.png' },
  { id: 2, brand: 'Honda', model: 'Civic', year: 2021, image: './image/civic.png' },
  { id: 3, brand: 'Ford', model: 'Mustang', year: 2019, image: './image/mustang.png' },
];

const Menu: React.FC<{ activeMenu: string; onMenuChange: (menu: string) => void }> = ({
  activeMenu,
  onMenuChange,
}) => {
  return (
    <nav>
      <ul className="menu">
        <li className={activeMenu === 'Inicio' ? 'active' : ''}>
          <button onClick={() => onMenuChange('Inicio')}>Inicio</button>
        </li>
        <li className={activeMenu === 'Contactos' ? 'active' : ''}>
          <button onClick={() => onMenuChange('Contactos')}>Contactos</button>
        </li>
        <li className={activeMenu === 'Informacion' ? 'active' : ''}>
          <button onClick={() => onMenuChange('Informacion')}>Información</button>
        </li>
      </ul>
    </nav>
  );
};

const CarList: React.FC = () => {
  return (
    <div>
      <h1>Lista de Automóviles</h1>
      <ul>
        {carData.map((car) => (
          <li key={car.id}>
            <div className="car-item">
              <div className="car-image">
                <img src={require(`${car.image}`).default} alt={car.model} />
              </div>
              <div className="car-details">
                <h2>{car.brand}</h2>
                <p>
                  <strong>Modelo:</strong> {car.model}
                </p>
                <p>
                  <strong>Año:</strong> {car.year}
                </p>
              </div>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
};

const Contact: React.FC = () => {
  return (
    <div>
      <h1>Contactos</h1>
      <p>Puedes contactarnos mediante los siguientes canales:</p>
      <ul>
        <li>Teléfono: 123-456-7890</li>
        <li>Email: contacto@example.com</li>
        <li>Dirección: Calle Principal, Ciudad</li>
      </ul>
    </div>
  );
};

const Information: React.FC = () => {
  return (
    <div>
      <h1>Información</h1>
      <p>[Modelo 1]: Sumérgete en la sofisticación y el lujo con nuestro primer
         modelo destacado. Este vehículo combina un diseño elegante con un rendimiento 
         excepcional, ofreciendo una experiencia de conducción incomparable. Con características de
         vanguardia, desde sistemas de asistencia al conductor hasta conectividad avanzada, este modelo te
         mantendrá a la vanguardia de la innovación automotriz.</p>
      <p>[Modelo 2]: Si buscas un automóvil versátil y dinámico, no busques más. Nuestro segundo modelo 
         destacado ofrece un equilibrio perfecto entre estilo, rendimiento y funcionalidad. Desde su impresionante 
         potencia hasta su amplio espacio interior, este vehículo está diseñado para adaptarse a tu estilo de vida 
         activo y aventurero. Prepárate para explorar nuevas rutas y disfrutar de cada viaje.</p>
      <p>[Modelo 3]: Si buscas una experiencia de conducción emocionante y eléctrica, nuestro tercer modelo destacado 
        es la opción ideal. Con tecnología de vanguardia y cero emisiones, este automóvil eléctrico revoluciona la forma 
        en que te desplazas. Disfruta de un rendimiento impresionante, una carga rápida y una autonomía que te permitirá 
        ir más lejos. Únete a la revolución verde y experimenta el futuro de la movilidad.</p>

       <p>Estos tres modelos son solo una muestra de la amplia gama de vehículos 
        nuevos que ofrecemos. Nuestro equipo de expertos en automóviles está listo 
        para brindarte asesoramiento personalizado, responder a tus preguntas y ayudarte 
        a encontrar el automóvil perfecto que se adapte a tus necesidades y preferencias.</p>
        <p>¡Explora nuestra página web y descubre el automóvil de tus sueños!</p>
    </div>
  );
};

const App: React.FC = () => {
  const [activeMenu, setActiveMenu] = useState('Inicio');

  const handleMenuChange = (menu: string) => {
    setActiveMenu(menu);
  };

  let content;

  if (activeMenu === 'Inicio') {
    content = <CarList />;
  } else if (activeMenu === 'Contactos') {
    content = <Contact />;
  } else if (activeMenu === 'Informacion') {
    content = <Information />;
  }

  return (
    <div className="App">
      <Menu activeMenu={activeMenu} onMenuChange={handleMenuChange} />
      {content}
    </div>
  );
};

export default App;

